io.stdout:setvbuf("no")

local Sprite = require('Sprite')
local Container = require('Container')
local Player = require('Player')
local Lighting = require('Lighting')
local root = Container:new() 

local g = {
	-- Properties
	control_allowed_ = true,
	camera_ = {
		x = 0, y = 0,
	},
	world = {
		left_ = 0,
		right_ = 2200,
		getLeft = function(self)
			return self.left_
		end,
		getRight = function(self)
			return self.right_
		end
	},
	lights = Lighting:new(),
	root = root,

	-- Methods
	getWidth = function(self)
		return 200
	end,
	getHeight = function(self)
		return 150
	end,
	getCameraXY = function(self)
		return self:getCameraX(), self:getCameraY()
	end,
	getCameraX = function(self)
		return self.camera_.x
	end,
	getCameraY = function(self)
		return self.camera_.y
	end,
	setCameraX = function(self, x)
		self.camera_.x = x
	end,
	setCameraY = function(self, y)
		self.camera_.y = y
	end,
	setCameraCenter = function(self, x, y)
		self:setCameraX(x - self:getWidth() / 2)
		self:setCameraY(y - self:getHeight() / 2)
	end,
	setCameraLeft = function(self, x)
		self:setCameraX(x)
	end,
	setCameraRight = function(self, x)
		self:setCameraX(x - self:getWidth())
	end,
	getCameraLeft = function(self, x)
		return self:getCameraX()
	end,
	getCameraRight = function(self, x)
		return self:getCameraX() + self:getWidth()
	end,
	isControlAllowed = function(self)
		return self.control_allowed_
	end,
	disableControl = function(self)
		self.control_allowed_ = false
	end,
	enableControl = function(self)
		self.control_allowed_ = true
	end,

	-- Coroutine script methods
	wait = function(self, seconds)
		while seconds > 0 do
			seconds = seconds - self.dt
			coroutine.yield()
		end
	end
}


local frame = love.graphics.newCanvas(g:getWidth(), g:getHeight())
frame:setFilter('nearest', 'nearest')
local grain = require('Grain'):new()


g.lights.ambient.r = 0
g.lights.ambient.g = 0
g.lights.ambient.b = 0


local function newEnvi()
	local envi = Container:new()
	envi.green_lights = {}
	for i = 1, 20 do
		local wall = Sprite:new('Wall/Wall_Diffuse.png', 1, 1, 'Wall/Normal.png')
		wall.node.x = g.world:getLeft() + (i - 1) * wall:getFrameWidth()
		wall.node.y = 7
		envi:addChild(wall)

		if i <= 3 then
			local l = g.lights:newLight()
			l.node.z = 0.04
			l.intensity = 7
			l.node.y = 18
			l.node.x = wall.node.x + 38
			l.color.r = 0.3
			l.color.g = 1
			l.color.b = 0.6
			l.multiplier = 1
			l.falloff = {3, 30, 30}
			envi:addChild(l)
			table.insert(envi.green_lights, l)

			local accum = 0
			envi:addChild({
				update = function(self, g)
					accum = accum + g.dt * math.random(1, 10) * 200
					if accum > 2 * math.pi then
						accum = 0
					end
					l.intensity = l.multiplier * (5 + 3 * math.sin(accum))

				end,
				draw = function() end
			})
		end
	end

	return envi
end

g.envi = newEnvi()
root:addChild(g.envi)

local player = Player:new(g)
player.node.y = 30
player.node.x = 265
root:addChild(player)

local mouse_light = nil
local SCREEN_SCALE = 4

function g:getPlayer()
	return player
end

--root:addChild(require('script2')(g))
--
--
local COLORS = {
	{0.3, 1, 0.6},
	{1, 0.3, 0.6},
	{0.3, 0.6, 1},
}
local cur_color = 1

function love.update(dt)
	g.dt = dt
	root:update(g)
--[[
	if player:getLeft() < g.world:getLeft() then
		player:setLeft(g.world:getLeft())
	elseif player:getRight() > g.world:getRight() then
		player:setRight(g.world:getRight())
	end
]]
	--g:setCameraCenter(player.node.x + g:getWidth() / 3, player.node.y + 14)
	g:setCameraCenter(player.node.x, player.node.y + 14)

	if g:getCameraLeft() < g.world:getLeft() then
		g:setCameraLeft(g.world:getLeft())
	elseif g:getCameraRight() > g.world:getRight() then
		g:setCameraRight(g.world:getRight())
	end

	root.node.x = -g:getCameraX()
	root.node.y = -g:getCameraY()

	if love.keyboard.isDown('space') then

		if not mouse_light and #(g.lights:getLights()) < Sprite.getLightCount() then
			mouse_light = g.lights:newLight()
			mouse_light.node.z = 0.04
			mouse_light.intensity = 7
			--mouse_light.node.y = 18
			--mouse_light.node.x = wall.node.x + 38
			local color = COLORS[cur_color]
			cur_color = cur_color + 1
			if cur_color > #COLORS then
				cur_color = 1
			end
			mouse_light.color.r = color[1]
			mouse_light.color.g = color[2]
			mouse_light.color.b = color[3]
			mouse_light.multiplier = 1
			mouse_light.user_added = true
			mouse_light.falloff = {3, 30, 30}
			g.envi:addChild(mouse_light)
		end

		if mouse_light then
			mouse_light.visible = true
			local mx, my = love.mouse.getPosition()
			mx, my = mx / SCREEN_SCALE, my / SCREEN_SCALE
			mouse_light.node:setGlobalXY(mx, my)
		end
	else
		if mouse_light then
			mouse_light.visible = false
		end
	end

	love.window.setTitle('Nine (fps: ' .. love.timer.getFPS() .. ')')

	grain:update(g)
end

function love.draw()

	love.graphics.setColor(255, 255, 255, 255)

	-- Draw stuff onto canvas
	love.graphics.setCanvas(frame)
	love.graphics.clear(0, 0, 0, 255)
	love.graphics.push()
	root:draw(g)
	love.graphics.pop()

	grain:draw(g)

	-- for i, v in ipairs(g.lights.lights) do
	-- 	love.graphics.setColor(v.color.r * 255, v.color.g * 255, v.color.b * 255, 255)
	-- 	local x, y, z = v.node:getGlobalXYZ()
	-- 	local radius = 2
	-- 	love.graphics.line(x - radius, y, x + radius, y)
	-- 	love.graphics.line(x, y - radius, x, y + radius)
	-- end

	love.graphics.setCanvas(nil)

	-- Draw the canvas
	love.graphics.push()
	love.graphics.scale(SCREEN_SCALE, SCREEN_SCALE)
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(frame)
	love.graphics.pop()
end

function love.mousepressed(x, y, button, itouch)
	if button == 1 then
		-- put mouse_light down into the world
		mouse_light = nil
	end
end

function love.keypressed(key, isrepeat)
	if key == 'escape' then
		love.event.quit()
	elseif key == 'l' then
		local lights = g.lights:getLights()
		for i, v in ipairs(lights) do
			if not v.user_added then
				v.visible = not v.visible
			end
		end
	end
end

