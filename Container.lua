
local class = require('class')
local Node = require('Node')
local Container = class('Container')

function Container:init()
	self.count = 0
	self.children = {}
	self.to_be_removed_ = {}
	self.node = Node:new()
end


function Container:update(g)
	local i = 1
	local child_removed = false
	while i <= #self.children do
		local v = self.children[i]
		if self.to_be_removed_[v] then
			table.remove(self.children, i)
			child_removed = true
		else
			self.children[i]:update(g)
			i = i + 1
		end
	end

	if child_removed then
		self.to_be_removed_ = {}
	end
end


function Container:draw(g)
	self.node:push()

	for i, k in ipairs(self.children) do
		self.children[i]:draw(g)
	end

	self.node:pop()
end


function Container:addChild(child)
	assert(child.update ~= nil, "")
	assert(child.draw ~= nil, "")
	self.count = self.count + 1
	self.children[self.count] = child

	if child.node then
		child.node:setParent(self.node)
	end
end

function Container:addChildAt(index, child)
	assert(child.update ~= nil, "")
	assert(child.draw ~= nil, "")
	self.count = self.count + 1
	table.insert(self.children, index, child)
end


function Container:removeChild(child)
	for i, v in ipairs(self.children) do
		if v == child then
			self.to_be_removed_[child] = true
		end
	end

	if child.node then
		child.node:setParent(nil)
	end
end


return Container