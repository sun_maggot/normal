local class = require('class')
local Node = require('Node')

local Sprite = class('Sprite')
local LIGHT_COUNT = 40
local function getShaderSrc(count)

	return [[

extern Image normal_map;

#define LIGHT_COUNT ]] .. count .. [[

extern vec3 light_pos[LIGHT_COUNT];
extern vec4 light_colors[LIGHT_COUNT];
extern vec3 falloffs[LIGHT_COUNT];
extern vec2 resolution;
extern vec4 ambient_color;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
	vec3 intensity = vec3(0, 0, 0);
	//vec3 falloff = vec3(0.2, 40, 40);

	for (int i = 0; i < LIGHT_COUNT; i++)
	{
		vec3 light_dir = vec3(light_pos[i].xy - (screen_coords / resolution), light_pos[i].z);
		light_dir.x *= resolution.x / resolution.y;
		vec3 normal = Texel(normal_map, texture_coords).rgb * 2.0 - 1.0;
		normal.y *= -1;

		number D = length(light_dir);
		vec3 N = normalize(normal);
		vec3 L = normalize(light_dir);

		vec3 diffuse = (light_colors[i].rgb * light_colors[i].a) * max(dot(N, L), 0.0);
		//number attenuation = 1.0 / (falloff.x + (falloff.y * D) + (falloff.z * D * D));
		number attenuation = 1.0 / (falloffs[i].x + (falloffs[i].y * D) + (falloffs[i].z * D * D));
		intensity += diffuse * attenuation;
	}
	vec3 ambient = ambient_color.rgb * ambient_color.a;
	intensity += ambient;

	//number level = 15;
	//intensity *= level;
	//intensity = floor(intensity);
	//intensity /= level - 0.5;

	vec4 tex = Texel(texture, texture_coords);
	vec3 finalColor = tex.rgb * intensity;

	return vec4(finalColor, tex.a);
}

]]

end

local cache = setmetatable({}, { __mode = 'v' })

local shader = love.graphics.newShader(getShaderSrc(LIGHT_COUNT))

function Sprite:init(path, num_cols, num_rows, npath)
	num_cols = num_cols or 1
	num_rows = num_rows or 1

	self.visible = true
	if cache[path] then
		self.image = cache[path]
	else
		self.image = love.graphics.newImage(path)
		self.image:setFilter('nearest', 'nearest')
		cache[path] = self.image
	end
	self.quad = love.graphics.newQuad(
		0, 0,
		self.image:getWidth() / num_cols,
		self.image:getHeight() / num_rows,
		self.image:getDimensions()
	)

	if npath then
		if cache[npath] then
			self.normal = cache[npath]
		else
			self.normal = love.graphics.newImage(npath)
			self.normal:setFilter('nearest', 'nearest')
			cache[npath] = self.normal
		end
	end

	self.node = Node:new()
	self.animations = {}
	self.index = 0
	self.curAnim = nil
	self.remaining = 0
end


function Sprite:update(dt)
	self:updateAnim(dt)
end


function Sprite:getFrameWidth()
	local _, _, w, _ = self.quad:getViewport()
	return w
end

function Sprite:getFrameHeight()
	local _, _, _, h = self.quad:getViewport()
	return h
end

function Sprite:getFrameX()
	local x, _, _, _ = self.quad:getViewport()
	return x
end

function Sprite:getFrameY()
	local _, y, _, _ = self.quad:getViewport()
	return y
end

function Sprite:getFrameCol()
	return math.floor(self:getFrameX() / self:getFrameWidth())
end

function Sprite:getFrameRow()
	return math.floor(self:getFrameY() / self:getFrameHeight())
end

function Sprite:getFrame()
	return self:getFrameRow() * math.floor(self.image:getWidth() / self:getFrameWidth()) + self:getFrameCol()
end

function Sprite:getNumCols()
	return math.floor(self.image:getWidth() / self:getFrameWidth())
end

function Sprite:getNumRows()
	return math.floor(self.image:getHeight() / self:getFrameHeight())
end

function Sprite:getFrame()
	return self:getFrameRow() * self:getNumCols() + self:getNumCols()
end

function Sprite:setFrame(frame)
	self.quad:setViewport(
		math.floor(frame % self:getNumCols()) * self:getFrameWidth(),
		math.floor(frame / self:getNumCols()) * self:getFrameHeight(),
		self:getFrameWidth(),
		self:getFrameHeight()
	)
end

function Sprite:addAnim(name, frames, fps, looped)
	self.animations[name] = {
		name = name,
		frames = frames,
		fps = fps,
		looped = looped,
		getFrameTime = function(self)
			return 1 / self.fps
		end,
		getNumFrames = function(self)
			return #self.frames
		end,
		isLooped = function(self)
			return self.looped
		end,
		getFrame = function(self, index)
			return self.frames[index]
		end
	}
end

function Sprite:play(name, forced)
	if self.curAnim ~= self.animations[name] or forced then
		self.curAnim = self.animations[name]
		self.index = 1
		self.remaining = self.curAnim:getFrameTime()
	end
end

function Sprite:updateAnim(dt)
	if self:isAnimating() then
		self.remaining = self.remaining - dt
		if self.remaining < 0 then
			self.remaining = self.curAnim:getFrameTime()
			self.index = self.index + 1
			if self.index > self.curAnim:getNumFrames() then
				if self.curAnim:isLooped() then
					self.index = 1
					self:setFrame( self.curAnim:getFrame(self.index) )
				else
					self:stop()
				end
			end
		else
			self:setFrame( self.curAnim:getFrame(self.index) )
		end
	end
end

function Sprite:isAnimating()
	return self.curAnim ~= nil
end

function Sprite:stop()
	self.curAnim = nil
end

function Sprite:draw(g)
	if not self.visible then
		return
	end
	self.node:push()

	if self.normal then

		local positions, colors, falloffs, ambient = g.lights:getUniformInfo(g)
		for i = 1, LIGHT_COUNT do
			if positions[i] == nil then
				positions[i] = { 0, 0, 0 }
			end
			if colors[i] == nil then
				colors[i] = { 0, 0, 0, 0 }
			end
			if falloffs[i] == nil then
				falloffs[i] = { 1, 1, 1 }
			end
		end

		love.graphics.setShader(shader)
		shader:send('normal_map', self.normal)
		shader:send('light_pos', unpack(positions))
		shader:send('light_colors', unpack(colors))
		shader:send('falloffs', unpack(falloffs))
		shader:send('ambient_color', ambient)
		shader:send('resolution', { g:getWidth(), g:getHeight() })
		love.graphics.draw(self.image, self.quad)
		love.graphics.setShader()
		
	else
		love.graphics.draw(self.image, self.quad, 0, 0, 0, 1, 1, 0.5, 0)
	end
	self.node:pop()
end

function Sprite.getLightCount()
	return LIGHT_COUNT
end

return Sprite
