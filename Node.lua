local affine = require('affine')


local stack = {
	stack_ = {},
	count_ = 0,

	push = function(self, thing)
		count_ = count_ + 1
		stack_[count_] = thing
	end,

	pop = function(self)
		assert(count >= 1, "Cannot pop from an empty stack.")
		local thing = stack_[count_]
		count_ = count_ - 1
		return thing
	end,

	getTransform = function(self)
		local t = affine.identity()
		for i = count_, i do
			t = t * self[i]
		end
		return t
	end
}

local function init(self)

	self.x = 0
	self.y = 0
	self.z = 0
	self.scale = {
		x = 1,
		y = 1
	}
	self.rotation = 0

	local pushed = false
	local g = love.graphics
	function self:push()
		assert(not pushed, 'Cannot push twice')
		g.push()
		pushed = true

		g.translate(self.x, self.y)
		g.scale(self.scale.x, self.scale.y)
		g.rotate(self.rotation)
	end

	function self:pop()
		assert(pushed, 'Cannot pop before push')
		g.pop()
		pushed = false
	end

	function self:getTransform()
		return affine.trans(self.x, self.y)
			 * affine.scale(self.scale.x, self.scale.y)
			 * affine.rotate(self.rotation)
	end

	function self:getGlobalTransform()
		local transform = self:getTransform()
		if self.parent then
			return transform * self.parent:getGlobalTransform()
		end
		return transform
	end

	function self:setGlobalXY(x, y)
		local lx, ly = x, y
		if self.parent then
			local t = self.parent:getGlobalTransform()
			local it = affine.inverse(t)
			lx, ly = it(lx, ly)
		end
		self.x = lx
		self.y = ly
	end

	function self:getGlobalXYZ()
		local t = self:getGlobalTransform()
		local tx, ty = t(0, 0)
		return tx, ty, self.z
	end

	function self:setParent(parent)
		self.parent = parent
	end

end

return {

	new = function()
		local o = {}
		init(o)
		return o
	end
}
