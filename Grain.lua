return {
	new = function()
		local o = {}
		local texture = love.graphics.newImage('grain.png')
		local x = 0

		local fps = 23
		local remaining = 0
		function o:update(g)
			remaining = remaining - g.dt
			if remaining < 0 then
				x = math.random(0, g:getWidth() - texture:getWidth())
				remaining = 1 / fps
			end
		end

		function o:draw(g)
			local bMode = love.graphics.getBlendMode()

			love.graphics.setBlendMode('multiply')
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.draw(texture, x)
			love.graphics.setBlendMode(bMode)
		end


		return o
	end
}
