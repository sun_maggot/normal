return function(g)
	local Sprite = require('Sprite')

	local function newHer()
		local her = Sprite:new('Her/Her_Diffuse.png', 1, 1, 'Her/Normal.png')
		her.node.y = 30
		return her
	end


	her1 = newHer()
	her1.node.x = 100
	her1.visible = false
	g.root:addChild(her1)
	her2 = newHer()
	her2.node.x = 100
	her2.visible = false
	g.root:addChild(her2)

	local co = coroutine.create(function(g)

		while true do
			coroutine.yield()
			if g:getPlayer():getRight() > 500 then
				g:disableControl()
				break
			end
		end

		g:wait(1)

		local faded_green_lights = false
		while not faded_green_lights do
			coroutine.yield()
			for i, v in ipairs(g.envi.green_lights) do
				v.multiplier = v.multiplier - g.dt
				if v.multiplier <= 0 then
					v.multiplier = 0
					faded_green_lights = true
				end
			end
		end

		g:wait(1)

		local YELLOW_BRIGHTNESS = 30

		local function newYellowLight()
			local y = g.lights:newLight()
			y.node.z = 0.2
			y.falloff = {7, 150, 200}
			y.intensity = 0
			y.color.r = 0.7
			y.color.g = 0.5
			y.color.b = 0.9

			local PLAYER_DIST = g:getWidth() * 0.7


			function y:resetPosition()
				local pcx, pcy = g:getPlayer():getCenter()
				y.node.x = math.floor(pcx + PLAYER_DIST)
				y.node.y = pcy - 70
			end

			function y:turnOff(time)
				yellow_accum = time
				while yellow_accum > 0 do
					coroutine.yield()
					yellow_accum = yellow_accum - g.dt
					y.intensity = YELLOW_BRIGHTNESS * yellow_accum / time
				end
			end

			function y:turnOn(time)
				local yellow_accum = 0
				while yellow_accum < time do
					coroutine.yield()
					yellow_accum = yellow_accum + g.dt
					y.intensity = YELLOW_BRIGHTNESS * yellow_accum / time
				end
			end

			function y:waitForPlayer()
				local target_intensity = y.intensity
				local total_distance = y.node.x - 30 - g:getPlayer():getRight()

				while true do
					coroutine.yield()

					local progress = (y.node.x - 30 - g:getPlayer():getRight()) / total_distance --(PLAYER_DIST + g:getPlayer():getWidth() / 2)
					progress = math.min(progress, 1)
					progress = math.max(progress, 0)
					y.intensity = target_intensity * progress

					if g:getPlayer():getRight() > y.node.x - 20 then
						break
					end
				end
			end

			return y
		end

		local function alternateLight(y1, y2, time)
			local yellow_accum = 0
			while yellow_accum < time do
				coroutine.yield()
				yellow_accum = yellow_accum + g.dt
				if yellow_accum >= time then
					yellow_accum = time
				end
				y2.intensity = YELLOW_BRIGHTNESS * yellow_accum / time
				y1.intensity = YELLOW_BRIGHTNESS * (1 - yellow_accum / time)
			end
		end

		local YELLOW_TOTAL_TIME = 0.7
		local YELLOW_QUICK_TIME = 0.2

		local y = newYellowLight()
		local y2 = newYellowLight()
		g.root:addChild(y)
		g.root:addChild(y2)

		local HER_LIGHT_DIST = -10

		y:resetPosition()
		her1.node.x = y.node.x + HER_LIGHT_DIST
		her1.visible = true
		y:turnOn(YELLOW_TOTAL_TIME)
		g:wait(1)
		g:enableControl()
		y:waitForPlayer()


		while true do
			-- y:turnOff(YELLOW_QUICK_TIME)
			y2:resetPosition()
			her1.node.x = y2.node.x + HER_LIGHT_DIST
			y2:turnOn(YELLOW_QUICK_TIME)
			y2:waitForPlayer()
			-- y2:turnOff(YELLOW_QUICK_TIME)
			y:resetPosition()
			her1.node.x = y.node.x + HER_LIGHT_DIST
			y:turnOn(YELLOW_QUICK_TIME)
			y:waitForPlayer()
		end







		-- y:turnOn(YELLOW_QUICK_TIME)
		-- y:waitForPlayer()
		-- y:turnOff(YELLOW_QUICK_TIME)
		-- y:resetPosition()
		-- y:turnOn(YELLOW_QUICK_TIME)





	end)

	return {
		update = function(self, g)
			if not co then return end
			local result, error = coroutine.resume(co, g)
			if result == false then
				print(error)
				co = nil
			end
		end,

		draw = function() end
	}

end