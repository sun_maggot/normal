local Matrix = {}
Matrix.__mul = function(a, b)
	assert(#a == 9 and #b == 9, "Matrix must have 9 elements")

	local o = {
		0, 0, 0,
		0, 0, 0,
		0, 0, 0
	}
	setmetatable(o, Matrix)
	for i, v in ipairs(o) do
		local row = math.floor((i - 1) / 3) + 1
		local col = math.floor((i - 1) % 3) + 1
		print(b:at(row, col))

		for j = 1, 3 do
			o[i] = o[i] + a:at(row, j) * b:at(j, col)
		end
	end
	return o
end
Matrix.__tostring = function(t)
	return t[1]..'\t'..t[2]..'\t'..t[3]..'\n'..t[4]..'\t'..t[5]..'\t'..t[6]..'\n'..t[7]..'\t'..t[8]..'\t'..t[9]..'\n'
end
Matrix.__index = Matrix

function Matrix:identity()
	local o = {
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
	}
	setmetatable(o, Matrix)
	return o
end


function Matrix:rotation(r)
	local o = {
		math.cos(r), math.sin(r), 0,
		-math.sin(r), math.cos(r), 0,
		0, 0, 0,
		0, 0, 1
	}
	setmetatable(o, Matrix)
	return o
end


function Matrix:translation(tx, ty)
	local o = {
		1, 0, tx,
		0, 1, ty,
		0, 0, 1
	}
	setmetatable(o, Matrix)
	return o
end


function Matrix:scale(sx, sy)
	local o = {
		sx, 0, 0,
		0, sy, 0,
		0, 0, 1
	}
	setmetatable(o, Matrix)
	return o
end



function Matrix:at(row, col)
	assert(row <= 3 and col <= 3 and row >= 1 and col >= 1, 'Row and col must be between 1 and 3')
	return self[(row - 1) * 3 + col]
end


function Matrix:inverse()
	local s = self
	local o = {
		s:at(2, 2) / 
	}
end

function Matrix:getDeterminant(this)
    return this:at(1, 1) * this:at(2, 2) - this:at(1, 2) * this:at(2, 1)
end


function Matrix:transform(v)
	return {
		v[1] * self[1] + v[2] * self[4] + v[3] * self[7],
		v[1] * self[2] + v[2] * self[5] + v[3] * self[8],
		v[1] * self[3] + v[2] * self[6] + v[3] * self[9]
	}
end

function Matrix:new(m00, m10, m01, m11, m02, m12)
	local o = {
		m00 = m00,
		m10 = m10,
		m01 = m01,
		m11 = m11,
		m02 = m02,
		m12 = m12
	}
	setmetatable(o, Matrix)
	return o
end




return Matrix