--
-- The basic idea is:
-- Parent class has three look-up tables, special, static, and instance_dict
-- When accessing properties from the class, we will look up in those three
-- tables in order, i.e. if found a value in special table, then return that value.
-- otherwise look in static table, if found a value, return that value otherwise
-- return the entry frm instance_dict
--
-- When creating an instance, the instance table's look-up is extended by
-- the class's instance_dict table. This table is intended to be give instance
-- access to instance methods.
--
-- When creating a subclass, the indexing of the static table will be extended
-- by the index of the parent's static table. Same goes for instance_dict.
--

--
-- Usage:
-- Call Object:sub([name of class]) to return a subclass
-- 
-- To create a constructor:
-- function Class:init([parameters])
--     ...
-- end
--
-- To create static methods/data:
-- 
-- function Class.static.method()
--     ...
-- end
--
-- Class.static.data = 'data'
-- 

local dict_table = setmetatable({}, { __mode = 'k' })


local function create_class(name, parent)
	local class = {}
	local static = {}
	local instance_dict = {}
	local special = { static = static, super = parent, name = name }
	dict_table[class] = instance_dict

	if parent then
		setmetatable(static, {
				__index = parent.static
			})
		setmetatable(instance_dict, {
				__index = dict_table[parent]
			})
	end

	setmetatable(class, {
		__index = function(_, key)
			do
				local value = special[key]
				if value then return value end
			end

			do
				local value = static[key]
				if value then return value end
			end

			return instance_dict[key]
		end,
		__newindex = instance_dict
	})

	function special:new(...)
		local o = setmetatable({}, {
				__index = instance_dict,
				__tostring = function()
					return 'instance of class '..class.name
				end
			})
		if o.init then
			o:init(...)
		end
		o.class = class
		return o
	end

	function special:sub(name)
		return create_class(name, class)
	end

	-- function instance_dict:init()
	-- end

	return class
end



return create_class