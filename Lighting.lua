
local class = require('class')
local Node = require('Node')

--[[
	Light class
	defined internally in Lighting. Can only
	be created by Lighting class
]]
local Light = class('Light')
function Light:init()
	self.node = Node:new()
	self.node.z = 0.1
	self.color = {
		r = 1,
		g = 1,
		b = 1
	}
	self.intensity = 10
	self.falloff = {3, 40, 40}
	self.visible = true
end
function Light:update(g) end
function Light:draw(g) end
function Light:getGlobalXY()
	local gx, gy, _ = self:getGlobalXYZ()
	return gx, gy
end


--[[
	Lighting class
	A colletion of lights. Can be used to
	supply uniform information to shader
]]
local Lighting = class('Lighting')
function Lighting:init()
	self.count = 0
	self.lights = {}
	self.ambient = {
		r = 1,
		g = 1,
		b = 1,
		a = 1
	}
end

function Lighting:newLight()
	local light = Light:new()
	self.count = self.count + 1
	self.lights[self.count] = light
	return light
end

function Lighting:removeLight(light)
	for i, v in ipairs(self.lights) do
		if v == light then
			table.remove(self.lights, i)
			self.count = self.count - 1
			return
		end
	end
end

function Lighting:getLights()
	return self.lights
end

function Lighting:getUniformInfo(g)
	local positions, colors, falloffs = {}, {}, {}
	for i, v in ipairs(self.lights) do
		if v.visible then
			local gx, gy, gz = v.node:getGlobalXYZ()
			positions[#positions + 1] = {
				gx / g:getWidth(), gy / g:getHeight(), gz
			}
			colors[#colors + 1] = {
				v.color.r, v.color.g, v.color.b, v.intensity
			}
			falloffs[#falloffs + 1] = {
				v.falloff[1], v.falloff[2], v.falloff[3]
			}
		end
	end
	local ambient = {self.ambient.r, self.ambient.g, self.ambient.b, self.ambient.a}
	return positions, colors, falloffs, ambient
end


return Lighting
