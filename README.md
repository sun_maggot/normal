# Normal #

![Alt text](https://media.giphy.com/media/3o7qDND2rxXxXj9YjK/giphy.gif)
![Alt text](https://media.giphy.com/media/3o7qEc5X9gbbieJq8M/giphy.gif)

Use Left and Right arrow to walk around.

Hold down space bar to create a light that follows your mouse. Left mouse button to put it down.

Don't be alarmed by all the classes! I was about to turn this into a bigger project. The shader is in Sprite.lua

# Running #

Download LÖVE here: https://love2d.org/. Drag the folder onto the LÖVE executable.