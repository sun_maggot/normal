local class = require('class')
local Sprite = require('Sprite')
local Node = require('Node')
local Player = class('Player')

local SPEED = 60
local FPS = SPEED / 10

function Player:init(g)

	self.node = Node:new()

	self.sprite_left = Sprite:new('Me/Me_Left_Diffuse.png', 8, 2, 'Me/Normal_Left.png')
	self.sprite_left:addAnim('standing', {0}, 8, true)
	self.sprite_left:addAnim('walking', {8, 9, 10, 11, 12, 13, 14, 15}, FPS, true)
	self.sprite_left:setFrame(7)
	self.sprite_left:play('walking')
	self.sprite_left.node.x = -self.sprite_left:getFrameWidth() / 2
	self.sprite = Sprite:new('Me/Me_Right_Diffuse.png', 8, 2, 'Me/Normal_Right.png')
	self.sprite:addAnim('standing', {0}, 8, true)
	self.sprite:addAnim('walking', {8, 9, 10, 11, 12, 13, 14, 15}, FPS, true)
	self.sprite:setFrame(7)
	self.sprite:play('walking')
	self.sprite.node.x = -self.sprite:getFrameWidth() / 2
	self.sprite.visible = false

end

function Player:getHeight()
	return self.sprite:getFrameHeight()
end

function Player:getWidth()
	return self.sprite:getFrameWidth() / 2
end

function Player:getLeft()
	return math.floor(self.node.x - self:getWidth() / 2)
end

function Player:getRight()
	return math.floor(self.node.x + self:getWidth() / 2)
end

function Player:setLeft(v)
	self.node.x = v + self:getWidth() / 2
end

function Player:setRight(v)
	self.node.x = v - self:getWidth() / 2
end

function Player:getCenter()
	return self.node.x, self.node.y + self:getHeight()
end


function Player:draw(g)
	self.node:push()

	self.sprite_left:draw(g)
	self.sprite:draw(g)

	self.node:pop()
end


function Player:update(g)
	local dt = g.dt


	if g:isControlAllowed() and love.keyboard.isDown('left') and not love.keyboard.isDown('right') then
		self.node.x = self.node.x - dt * SPEED
		self.sprite_left:play('walking')
		self:faceLeft()
	elseif g:isControlAllowed() and love.keyboard.isDown('right') and not love.keyboard.isDown('left') then
		self.node.x = self.node.x + dt * SPEED
		self.sprite:play('walking')
		self:faceRight()
	else
		self.sprite:play('standing')
		self.sprite_left:play('standing')
	end

	self.sprite_left:update(dt)
	self.sprite:update(dt)
end


function Player:faceLeft()
	self.sprite.visible = false
	self.sprite_left.visible = true
end

function Player:faceRight()
	self.sprite.visible = true
	self.sprite_left.visible = false
end



return Player