return function(g)

	
	local state = 'walking'
	local timer = 0
	local states = {}
	local curState = nil
	local function changeState(name, ...)
		if curState and curState.stop then
			curState.stop()
		end
		curState = states[name]
		assert(curState, 'state '..name..' doesn\'t exist.')
		if curState.start then
			curState.start(...)
		end
	end

	local function addState(name, update, start, stop)
		states[name] = {
			start = start,
			update = update,
			stop = stop
		}
	end

	-- Util states
	local wait_remaining_ = 0
	local wait_next_state_ = 0
	local wait_next_state_args_ = {}
	local wait_next_state_name_ = ''
	addState('wait', function(g)
		wait_remaining = wait_remaining_ - g.dt
		if wait_remaining_ < 0 then
			changeState(wait_next_state_name_, unpack(wait_next_state_args_))
		end
	end,
	function(seconds, next_state, ...)
		wait_remaining_ = seconds
		wait_next_state_name_ = next_state
		wait_next_state_args_ = {...}
	end)


	-- Here it starts
	addState('walking', function(g)
		if g:getPlayer():getRight() > 180 then
			g:disableControl()
			changeState('wait', 1, 'green_lights_fade')
		end
	end)

	addState('green_lights_fade', function(g)
		local fade_speed = 4
		local done = false
		for i, v in ipairs(g.green_lights) do
			v.multiplier = v.multiplier - fade_speed * g.dt
			if v.multiplier < 0 then
				v.multiplier = 0
				done = true
			end
		end
		if done then
			changeState('wait', 2, 'yellow_on')
		end
	end)


	local yellow_b = 0
	local yellow_light = nil
	addState('yellow_on', function(g)
		yellow_light.intensity = yellow_light.intensity + g.dt
		if yellow_light.intensity > 1
			yellow_light.intensity = 1
			changeState('wait', 2, 'yellow_off')
		end
	end,
	function()
		yellow_b = 0
		local l = g.lights:newLight()
		g.root:addChild(l)
		local playerCenterX, playerCenterY = g:getPlayer():getCenter()
		l.node.x = playerCenterX + g:getWidth() / 2
		l.node.y = playerCenterY
		l.color.r = 1
		l.color.g = 1
		l.color.b = 0
		l.intensity = yellow_b
		yellow_light = l
	end)

	addState('yellow_off', function(g)
		yellow_light.intensity = yellow_light.intensity + g.dt
		if yellow_light.intensity > 1
			yellow_light.intensity = 1
			changeState('wait', 2, 'yellow_off')
		end
	end,

	changeState('walking')

	return {

		update = function(self, g)
			if curState then
				curState.update(g)
			end
		end,

		draw = function(self, g)
		end
	}
end